docker-bamboo-agent
---

**What's included in the box:**

* bamgoo-agent (surprise!)
* bash / curl / wget
* git
* ssh
* docker client
