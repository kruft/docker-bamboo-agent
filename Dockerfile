FROM adoptopenjdk/openjdk8:alpine-jre

MAINTAINER Dmitry Gadeev

ENV RUN_USER            bamboo
ENV RUN_GROUP           bamboo

ENV RUN_UID             1000
ENV RUN_GID             1000

ENV BAMBOO_AGENT_HOME           /var/atlassian/application-data/bamboo-agent
ENV BAMBOO_AGENT_INSTALL_DIR    /opt/atlassian/bamboo-agent

VOLUME ["${BAMBOO_AGENT_HOME}"]

WORKDIR $BAMBOO_AGENT_INSTALL_DIR

CMD ["/entrypoint.sh", "console"]
ENTRYPOINT ["/sbin/tini", "--"]

ARG DOCKER_VERSION=18.09.4
#ARG GIT_LFS_DOWNLOAD_URL=https://github.com/github/git-lfs/releases/download/v${GIT_LFS_VERSION}/git-lfs-linux-amd64-${GIT_LFS_VERSION}.tar.gz

#    && curl -L --silent ${GIT_LFS_DOWNLOAD_URL} | tar -xz --strip-components=1 -C "/usr/bin" git-lfs-${GIT_LFS_VERSION}/git-lfs \

RUN apk add --no-cache \
      ca-certificates \
      wget \
      curl \
      git \
      openssh \
      bash \
      procps \
#      openssl \
#      perl \
#      ttf-dejavu \
      tini \
    && addgroup -S ${RUN_GROUP} \
    && adduser -S -G ${RUN_GROUP} ${RUN_USER} \
    && wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
    && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.29-r0/glibc-2.29-r0.apk \
    && apk add glibc-2.29-r0.apk \
    && rm glibc-2.29-r0.apk \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/ /tmp/* /var/tmp/* \
    && curl -sSL https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz | tar zxf - -C /usr/local/bin/ --strip-components 1 docker

COPY entrypoint.sh              /entrypoint.sh

ARG BAMBOO_VERSION=6.9.0-rc1
ARG DOWNLOAD_URL=https://packages.atlassian.com/maven-closedsource-local/com/atlassian/bamboo/atlassian-bamboo-agent-installer/${BAMBOO_VERSION}/atlassian-bamboo-agent-installer-${BAMBOO_VERSION}.jar
ENV AGENT_JAR=atlassian-bamboo-agent-installer-${BAMBOO_VERSION}.jar
#ARG AGENT_JAR_TARGZ_PATH=atlassian-bamboo-${BAMBOO_VERSION}/atlassian-bamboo/admin/agent/${AGENT_JAR}


RUN mkdir -p ${BAMBOO_AGENT_INSTALL_DIR} \
    && curl -sSL -o ${BAMBOO_AGENT_INSTALL_DIR}/atlassian-bamboo-agent-installer-${BAMBOO_VERSION}.jar ${DOWNLOAD_URL} \
    && chown -R ${RUN_USER}:${RUN_GROUP}    ${BAMBOO_AGENT_INSTALL_DIR}
