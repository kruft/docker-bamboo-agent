#!/bin/bash

set -euo pipefail

: ${BAMBOO_SERVER:=}
: ${JAVA_OPTS:=}
: ${TOKEN:=}
: ${BAMBOO_AGENT_UUID:=}

BAMBOO_AGENT_HOSTNAME=$(hostname -i)

JAVA_OPTS="${JAVA_OPTS} -Dbamboo.home=${BAMBOO_AGENT_HOME}"

if [ -n "${TOKEN}" ]; then
    AGENT_OPTS="${BAMBOO_SERVER} -t ${TOKEN} $@"
else
    AGENT_OPTS="${BAMBOO_SERVER} $@"
fi

AGENT_RUN_COMMAND="java ${JAVA_OPTS} -jar ${AGENT_JAR} ${AGENT_OPTS}"

if [[ ${RUN_GID} != "" ]]; then
  sed -i -r "s|${RUN_GROUP}:x:[^:]+:${RUN_USER}|${RUN_GROUP}:x:${RUN_GID}:${RUN_USER}|" /etc/group
  sed -i -r "s|${RUN_USER}:x:([^:]+):[^:]+:(.*)|${RUN_USER}:x:\1:${RUN_GID}:\2|" /etc/passwd
fi

if [[ ${RUN_UID} != "" ]]; then
  sed -i -r "s|${RUN_USER}:x:[^:]+:(.*)|${RUN_USER}:x:${RUN_UID}:\1|" /etc/passwd
fi

if [[ "${BAMBOO_AGENT_UUID}" != "" ]]; then
  if [[ ! -f ${BAMBOO_AGENT_HOME}/bamboo-agent.cfg.xml ]]; then
  	echo "Creating agent configuration file..."
  	
  	echo 'agentUuid='${BAMBOO_AGENT_UUID} > ${BAMBOO_AGENT_HOME}/uuid-temp.properties
  else
    sed -ri "s|<agentUuid>[^<]+</agentUuid>|<agentUuid>${BAMBOO_AGENT_UUID}</agentUuid>|" ${BAMBOO_AGENT_HOME}/bamboo-agent.cfg.xml
  fi
fi

if [[ "${BAMBOO_AGENT_NAME}" != "" ]]; then
  if [[ -f ${BAMBOO_AGENT_HOME}/bamboo-agent.cfg.xml ]]; then
    sed -ri "s|<name>[^<]+</name>|<name>${BAMBOO_AGENT_NAME}</name>|" ${BAMBOO_AGENT_HOME}/bamboo-agent.cfg.xml
    sed -ri "s|<description>[^<]+</description>|<description>${BAMBOO_AGENT_NAME} remote agent @ ${BAMBOO_AGENT_HOSTNAME}</description>|" ${BAMBOO_AGENT_HOME}/bamboo-agent.cfg.xml
  fi
fi

# Start Bamboo as the correct user
if [ "${UID}" -eq 0 ]; then
    echo "User is currently root. Will change directories to daemon control, then downgrade permission to daemon"

    chmod -R 700 "${BAMBOO_AGENT_HOME}" &&
      chown -R "${RUN_USER}:${RUN_GROUP}" "${BAMBOO_AGENT_HOME}"

    # ACL...
    setfacl -LR -m "user:${RUN_USER}:rwx" ${BAMBOO_AGENT_HOME}/xml-data
    setfacl -LR -m "group:${RUN_GROUP}:rwx" ${BAMBOO_AGENT_HOME}/xml-data
    
    setfacl -LR -dm "user:${RUN_USER}:rwx" ${BAMBOO_AGENT_HOME}/xml-data
    setfacl -LR -dm "group:${RUN_GROUP}:rwx" ${BAMBOO_AGENT_HOME}/xml-data

    # Now drop privileges
    exec su -s /bin/bash "${RUN_USER}" -c "${AGENT_RUN_COMMAND}"
else
    exec "${AGENT_RUN_COMMAND}"
fi